// ==UserScript==
// @name        MusicBrainz: Import videos from YouTube
// @version     2013-12-16
// @author      -
// @namespace   df069240-fe79-11dc-95ff-0800200c9a66
//
// @include     *://www.youtube.com/watch?*
// ==/UserScript==
//**************************************************************************//

//alert("loaded");

var myform = document.createElement("form");
myform.method="get";
myform.action = "https://beta.musicbrainz.org/recording/create";
myform.acceptCharset = "UTF-8";
mysubmit = document.createElement("input");
mysubmit.type = "submit";
mysubmit.value = "Add to MusicBrainz";
myform.appendChild(mysubmit);

var div = document.createElement("div");
div.style.position = 'absolute';
div.style.top = 0;
div.style.right = 0;
div.style.padding = '20px';
div.style.margin = '50px';
div.style.zIndex = '10000';

var m = document.location.href.match(/\?v=([A-Za-z0-9_-]{11})/);
if (m && m[1]) {
	var yt_ws_url = "http://gdata.youtube.com/feeds/api/videos/" + m[1] + "?alt=json";
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open('GET', yt_ws_url, true);
	xmlhttp.onreadystatechange = function() { yt_callback(xmlhttp); }
	xmlhttp.send(null);
}

function yt_callback(req) {
	if (req.readyState != 4)
		return;
//	console.log(req.responseText);
	var r = eval('(' + req.responseText + ')');

	var video_id = r.entry.id["$t"];
	video_id = video_id.replace(/.*\//, "");

	var title = r.entry.title["$t"];
//	r.entry["media$group"]["media$title"]["$t"];

	var artist = r.entry.author[0].name["$t"];

	var artist_id = r.entry.author[0].uri["$t"];
	artist_id = artist_id.replace(/.*\//, "");

//	alert(r.entry["media$group"]["media$content"][0].medium);

	var length = r.entry["media$group"]["media$content"][0].duration;
//	r.entry["media$group"]["yt$duration"].seconds;
	var min = Math.floor(length / 60);
	var sec = length % 60

	add_field("edit-recording.name", title);
	add_field("edit-recording.video", 1);
	add_field("edit-recording.length", min + ":" + sec);
	add_field("edit-recording.edit_note", document.location.href);
	add_field("edit-recording.artist_credit.names.0.artist.name", artist);
	add_field("edit-recording.url.0.link_type_id", "268");
	add_field("edit-recording.url.0.text", document.location.href);

	var mb_ws_url = "http://musicbrainz.org/ws/2/url/?inc=artist-rels&fmt=json&resource=http://www.youtube.com/watch%3Fv=" + video_id;
	var xmlhttp2 = new XMLHttpRequest();
	xmlhttp2.open('GET', mb_ws_url, true);
	xmlhttp2.onreadystatechange = function() { mb_callback(xmlhttp2); }
	xmlhttp2.send(null);
}

function mb_callback(req) {
	if (req.readyState != 4)
		return;
//	console.log(req.responseText);
	var r = eval('(' + req.responseText + ')');

	if (r.relations) {
		div.style.backgroundColor = 'lightgreen';
		div.innerHTML = "Already in MB :D";
		document.body.appendChild(div);
	} else {
		div.style.backgroundColor = 'pink';
		div.appendChild(myform);
	}

	document.body.appendChild(div);
}

function add_field (name, value) {
//	value = cleanup(value);

	var field = document.createElement("input");
	field.type = "hidden";
	field.name = name;
	field.value = value;
	myform.appendChild(field);
}

